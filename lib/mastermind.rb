class Code
  attr_reader :pegs

  PEGS = {"R" => :red, "G" => :green, "B" => :blue, "Y" => :yellow,
    "O" => :orange, "P" => :purple}

  def self.parse(input)

    pegs = input.chars.map do |color|
      raise "Error, try again" if !PEGS.include?(color.upcase)

      PEGS[color.upcase]
    end

    Code.new(pegs)
  end

  def self.random
    pegs = []

    4.times {pegs << PEGS.values.shuffle[0]}

    Code.new(pegs)

  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(other_code)
    exact_match = 0
    pegs.each_index do |idx|
      exact_match += 1 if pegs[idx] == other_code[idx]
    end
    exact_match


  end

  def near_matches(other_code)
    other_color_counts = other_code.color_counts

    near_match = 0

    self.color_counts.each do |color, count|
      next unless other_color_counts.has_key?(color)

      near_match += [count, other_color_counts[color]].min
    end

    near_match - self.exact_matches(other_code)
  end

  def ==(other_code)
    return false unless other_code.is_a?(Code)
    self.pegs == other_code.pegs
  end

  protected

  def color_counts
    color_count = Hash.new(0)

    @pegs.each do |color|
      color_count[color] += 1
    end

    color_count
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Guess the code"

    begin
      Code.parse(gets.chomp)
    rescue
      puts "Error parsing code"
      retry
    end
  end

  def display_matches(guess)
    exact_matches = secret_code.exact_matches(guess)
    near_matches = secret_code.near_matches(guess)

    puts "You have #{exact_matches} exact matches"
    puts "You have #{near_matches} near matches"
  end

  def play
    
    10.times do
      guess = get_guess

      if guess == secret_code
        puts "You did it"
        return
      end
        display_matches(guess)
    end
    puts "You failed"
  end
end


if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
